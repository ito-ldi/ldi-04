/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.conversion;

/**
 *
 * @author Luis
 */
public class BinarioAHexadecimal {
    private static final char a = 'A';
    private static final char b = 'B';
    private static final char c = 'C';
    private static final char d = 'D';
    private static final char e = 'E';
    private static final char f = 'F';
    
    private String numBinario;
    
    public BinarioAHexadecimal(){}
    
    public String binarioAHexadecimal(String numBinario){
        String[] valores = new String[numBinario.length()];
        int n = 0;
         String suma = "";
         int sum = 0;
         double multi = 0;
         int exponente = 0;
         String resultado ="";        
         for(int x= numBinario.length(); x > 0; x--){
            String numBina = numBinario.substring(x-1, x);           
            int numBi = Integer.parseInt(numBina);            
            multi = Math.pow(2,exponente);            
            sum += (numBi * multi);            
            exponente++;             
            if(exponente == 4){              
                 if(sum > 9){
                         if(sum == 10){ suma = a+"";}
                    else if(sum == 11){ suma = b+"";}
                    else if(sum == 12){ suma = c+"";}
                    else if(sum == 13){ suma = d+"";}
                    else if(sum == 14){ suma = e+"";}
                    else if(sum == 15){ suma = f+"";}   
                    valores[n] = suma;
                }else{ 
                      valores[n] = sum+"";                     
                 }
                 exponente = 0;                
                 sum = 0;
                 n++;
            }           
        }        
         if(sum <= 7 && sum >0){    
            valores[n] = sum+"";
        }
         for(int x = valores.length-1; x >= 0; x--){   
             if(!(valores[x] == null)){
               resultado += valores[x];
               
         }
         }
         return resultado;
    }      
    public String getNumBinario() {
        return numBinario;
    }
    public void setNumBinario(String numBinario) {
        this.numBinario = numBinario;
    }   
    
  public static void main(String[] args){
      BinarioAHexadecimal conversion = new BinarioAHexadecimal();
      conversion.setNumBinario("000101101010011110010100");
      
      System.out.println("Convertidor de Número Binario a Hexadecimal");
      System.out.println("Ejemplo del libro");
      System.out.println("Número Binario = "+conversion.getNumBinario()+" = "
                        +conversion.binarioAHexadecimal(conversion.getNumBinario())+" Número Hexadecimal");
  }
    
    
}
